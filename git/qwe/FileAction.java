import java.io.IOException;

class FileAction {
	String fileName = "";
	int fileSize = 0;
	String fileEnd = ".txt";
	String newFileEnd = ".new";

	boolean exists = false;
	File file = null;
	BufferedReader buf = null;

	FileAction(String fname) {
		fileName = fName;
		file = new File(fileName);
		exists = file.exists();
	}

	public boolean Create() {
		boolean r = false;
		int i = 0;
		while(!exists | i < 5) {
				fileName = fileName + fileEnd;
				file = new File(fileName);
				exists = file.exists();
				i++;
			}
		if(!exists) {
			try {
				file.createNewFile();
			} catch (IOException IOEx) {
				System.out.println("Error");
			}
		}
	}

	public String Write(String data) {
		PrintWriter out = new PrintWriter(file.getAbsoluteFile());
		try {
			out.print(data);
		} catch (IOException t) {
			System.out.println("IOException");
		} finally {
			out.close();
		}
	}



}