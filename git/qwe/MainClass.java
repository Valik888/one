import java.io.File;
import java.util.Scanner;
import java.io.IOException;

class MainClass{
	public static void main(String[] args) {
		String fileName = read();
		create(fileName);	
	}

	public static void println(String p) {
		System.out.println(p);
	}

	public static String readFileName() {
		String fileName = "";
		println("File name: ");
		Scanner s = new Scanner(System.in);
		String str = s.nextLine();	
	}

	public static void create(String a) {
		File f = new File(a);
		String b = a;
		boolean exists = f.exists();
		if(a == "" | a == null | a.length() > 64) {
			a = "New file";	
		}
		while(exists) {
			b = b + ".new";
			File file = new File(b);
			exists = file.exists();
		}
		file.createNewFile();
	}

}